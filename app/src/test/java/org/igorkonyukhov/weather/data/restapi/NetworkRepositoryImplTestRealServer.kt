package org.igorkonyukhov.weather.data.restapi

import android.os.Build
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import org.igorkonyukhov.weather.data.gson.OpenWeatherJSON
import org.igorkonyukhov.weather.di.OpenWeatherServiceModule
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(AndroidJUnit4::class)
@Config(sdk = [Build.VERSION_CODES.O_MR1])
class NetworkRepositoryImplTestRealServer {
    private val lat = 52.036036
    private val lon = 113.438996
    private lateinit var networkRepositoryImpl: NetworkRepositoryImpl

    @Before
    fun setUp() {
        val weatherService = Retrofit.Builder()
            .baseUrl(OpenWeatherServiceModule.baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(OpenWeatherService::class.java)
        networkRepositoryImpl = NetworkRepositoryImpl(weatherService)
    }

    @After
    fun tearDown() {
        networkRepositoryImpl.cancelCurrentCall()
    }

    @Test
    fun getCurrentWeather_Success() {
        val apiCall = networkRepositoryImpl.createCall(lat, lon)

        val response = apiCall.execute()

        assertThat(response.isSuccessful).isTrue()
        val openWeatherJSON = response.body()
        assertThat(openWeatherJSON).isNotNull()
        assertThat(response.message()).isEqualTo("OK")
        assertThat(openWeatherJSON).isInstanceOf(OpenWeatherJSON::class.java)
    }
}