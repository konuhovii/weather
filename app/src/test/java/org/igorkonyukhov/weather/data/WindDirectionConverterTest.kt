package org.igorkonyukhov.weather.data

import org.junit.Test
import com.google.common.truth.Truth.assertThat
import org.igorkonyukhov.weather.util.WindDirectionConverter

class WindDirectionConverterTest {
    @Test
    fun convertDegreesToString() {
        assertThat(WindDirectionConverter.convert(0)).isEqualTo("North")
    }
}