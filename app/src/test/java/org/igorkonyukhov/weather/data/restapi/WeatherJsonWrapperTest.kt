package org.igorkonyukhov.weather.data.restapi

import android.webkit.URLUtil
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.google.common.truth.Truth.assertThat
import org.json.JSONException
import org.json.JSONObject
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class WeatherJsonWrapperTest {
    companion object {
        const val json =
            "{\"lat\":52.04,\"lon\":113.44,\"timezone\":\"Asia/Chita\",\"timezone_offset\":32400,\"current\":{\"dt\":1594621895,\"sunrise\":1594585292,\"sunset\":1594644121,\"temp\":24,\"feels_like\":24.88,\"pressure\":1010,\"humidity\":64,\"dew_point\":16.77,\"uvi\":8.04,\"clouds\":40,\"visibility\":10000,\"wind_speed\":2,\"wind_deg\":160,\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}]}}"
    }

    private lateinit var weatherJsonWrapper: WeatherJsonWrapper

    @Before
    fun setUp() {
        try {
            weatherJsonWrapper = WeatherJsonWrapper(JSONObject(json))
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    @Test
    fun getTemp_isNotNullAndIsInt() {
        assertThat(weatherJsonWrapper.temp).isNotNull()
        assertThat(weatherJsonWrapper.temp).isInstanceOf(java.lang.Integer::class.java)
    }

    @Test
    fun getWindSpeed_isNotNullAndIsDouble() {
        assertThat(weatherJsonWrapper.windSpeed).isNotNull()
        assertThat(weatherJsonWrapper.windSpeed).isInstanceOf(java.lang.Double::class.java)
    }

    @Test
    fun getWindDeg_isNotNullAndIsInt() {
        assertThat(weatherJsonWrapper.windDeg).isNotNull()
        assertThat(weatherJsonWrapper.windDeg).isInstanceOf(java.lang.Integer::class.java)
    }

    @Test
    fun getIconUrl_isNotNullAndIsStringAndIsValid() {
        val iconUrl = weatherJsonWrapper.iconUrl
        assertThat(iconUrl).isNotNull()
        assertThat(iconUrl).isInstanceOf(java.lang.String::class.java)
        assertThat(iconUrl).isNotEmpty()
        assertThat(URLUtil.isValidUrl(iconUrl)).isEqualTo(true)
    }
}