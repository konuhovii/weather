package org.igorkonyukhov.weather.data

import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.igorkonyukhov.weather.util.TaskRepeaterImpl
import org.junit.Test

@ExperimentalCoroutinesApi
class TaskRepeaterImplTest {
    @Test
    fun scheduleTask_taskRepeating() = runBlockingTest {
        val taskRepeater = TaskRepeaterImpl.getInstance(this)
        val delayAfter = 100L
        var i = 0
        taskRepeater.repeatTask(delayAfter) {
            i++
        }
        taskRepeater.cancelTask()
        assertThat(i).isGreaterThan(0)
    }
}