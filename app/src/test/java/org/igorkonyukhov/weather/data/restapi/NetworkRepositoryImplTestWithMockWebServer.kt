package org.igorkonyukhov.weather.data.restapi

import com.google.common.truth.Truth.assertThat
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.json.JSONObject
import org.junit.After
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import java.net.HttpURLConnection

class NetworkRepositoryImplTestWithMockWebServer {
    private lateinit var networkRepositoryImpl: NetworkRepositoryImpl
    private lateinit var mockWebServer: MockWebServer
    private val baseUrl = "/"
    private val successfulMockResponse = MockResponse()
        .setResponseCode(HttpURLConnection.HTTP_OK)
        .setBody(WeatherJsonWrapperTest.json)
    private val failureMockResponse = MockResponse()
        .setResponseCode(HttpURLConnection.HTTP_NOT_FOUND)

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start()
        val weatherService = Retrofit.Builder()
            .baseUrl(mockWebServer.url(baseUrl))
            .build()
            .create(OpenWeatherService::class.java)
        networkRepositoryImpl = NetworkRepositoryImpl(weatherService)
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test
    fun getCurrentWeather_Success() {
        mockWebServer.enqueue(successfulMockResponse)

        val apiCall = networkRepositoryImpl.createCall(0.0, 0.0)

        val response = apiCall.execute()

        assertThat(response.isSuccessful).isTrue()
        assertThat(response.body()).isNotNull()
        val jsonStr = response.body().toString()
        assertThat(jsonStr).isNotEmpty()
        assertThat(JSONObject(jsonStr)).isInstanceOf(JSONObject::class.java)
    }

    @Test
    fun getCurrentWeather_Failure() {
        mockWebServer.enqueue(failureMockResponse)

        val apiCall = networkRepositoryImpl.createCall(0.0, 0.0)

        val response = apiCall.execute()

        assertThat(response.isSuccessful).isFalse()
    }
}