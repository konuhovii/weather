package org.igorkonyukhov.weather.di

import dagger.Binds
import dagger.Module
import org.igorkonyukhov.weather.ui.viewmodel.LocalRepository
import org.igorkonyukhov.weather.data.room.LocalRepositoryImpl
import org.igorkonyukhov.weather.data.room.WeatherEntity

@Module
abstract class LocalRepositoryModule {
    @Binds
    abstract fun provideLocalRepository(localRepositoryImpl: LocalRepositoryImpl): LocalRepository
}