package org.igorkonyukhov.weather.di

import android.app.Application
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import org.igorkonyukhov.weather.ui.MainActivity
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        OpenWeatherServiceModule::class,
        RoomDatabaseModule::class,
        LocalRepositoryModule::class,
        NetworkRepositoryModule::class,
        ImageLoaderModule::class
    ]
)
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance context: Context,
            @BindsInstance app: Application
        ): AppComponent
    }

    fun inject(activity: MainActivity)
}