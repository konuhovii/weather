package org.igorkonyukhov.weather.di

import dagger.Binds
import dagger.Module
import org.igorkonyukhov.weather.ui.ImageLoader
import org.igorkonyukhov.weather.ui.ImageLoaderImpl
import javax.inject.Singleton

@Module
abstract class ImageLoaderModule {
    @Singleton
    @Binds
    abstract fun provideImageLoader(imageLoaderImpl: ImageLoaderImpl): ImageLoader
}