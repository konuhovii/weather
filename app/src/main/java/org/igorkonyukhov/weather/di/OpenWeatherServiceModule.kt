package org.igorkonyukhov.weather.di

import dagger.Module
import dagger.Provides
import org.igorkonyukhov.weather.data.restapi.OpenWeatherService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class OpenWeatherServiceModule {
    companion object {
        const val baseUrl = "https://api.openweathermap.org/data/2.5/"
    }

    @Singleton
    @Provides
    fun provideOpenWeatherService(): OpenWeatherService {
        return Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(OpenWeatherService::class.java)
    }
}