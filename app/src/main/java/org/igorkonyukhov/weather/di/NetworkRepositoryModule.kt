package org.igorkonyukhov.weather.di

import dagger.Binds
import dagger.Module
import org.igorkonyukhov.weather.ui.viewmodel.NetworkRepository
import org.igorkonyukhov.weather.data.restapi.NetworkRepositoryImpl
import javax.inject.Singleton

@Module
abstract class NetworkRepositoryModule {
    @Singleton
    @Binds
    abstract fun provideNetworkRepository(networkRepositoryImpl: NetworkRepositoryImpl): NetworkRepository
}