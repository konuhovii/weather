package org.igorkonyukhov.weather.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import org.igorkonyukhov.weather.data.room.WeatherDatabase

@Module
class RoomDatabaseModule {
    @Provides
    fun provideRoomDatabase(context: Context): WeatherDatabase {
        return Room.databaseBuilder(
            context,
            WeatherDatabase::class.java,
            "weather-database"
        ).fallbackToDestructiveMigration().build()
    }
}