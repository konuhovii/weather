package org.igorkonyukhov.weather.data.restapi

import org.igorkonyukhov.weather.data.gson.OpenWeatherJSON
import org.igorkonyukhov.weather.ui.viewmodel.NetworkRepository
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class NetworkRepositoryImpl @Inject constructor(private var openWeatherService: OpenWeatherService) :
    NetworkRepository {
    private val apiKey = "56b106d95c708d51a2937e26a4dad43b"
    private val exclude = "minutely,hourly,daily"
    private val units = "metric"

    var currentCall: Call<OpenWeatherJSON>? = null
        private set

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun getCurrentWeather(
        lat: Double,
        lon: Double
    ): Response<OpenWeatherJSON>? {
        if (currentCall != null)
            cancelCurrentCall()
        currentCall = createCall(lat, lon)
        return currentCall?.execute()
    }

    fun createCall(
        lat: Double,
        lon: Double
    ): Call<OpenWeatherJSON> {
        return openWeatherService.getWeather(
            lat, lon,
            exclude,
            units,
            apiKey
        )
    }

    override fun cancelCurrentCall() {
        if (currentCall?.isCanceled != true)
            currentCall?.cancel()
    }
}