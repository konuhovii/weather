package org.igorkonyukhov.weather.data.gson

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class OpenWeatherJSON {
    @SerializedName("lat")
    @Expose
    var lat: Double? = null

    @SerializedName("lon")
    @Expose
    var lon: Double? = null

    @SerializedName("timezone")
    @Expose
    var timezone: String? = null

    @SerializedName("timezone_offset")
    @Expose
    var timezoneOffset: Long? = null

    @SerializedName("current")
    @Expose
    var current: Current? = null
}