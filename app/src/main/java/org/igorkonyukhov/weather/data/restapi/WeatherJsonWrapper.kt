package org.igorkonyukhov.weather.data.restapi

import org.json.JSONObject

class WeatherJsonWrapper(jsonObject: JSONObject) {
    private val current = jsonObject.getJSONObject("current")

    private val weather0: JSONObject
        get() = current.getJSONArray("weather").getJSONObject(0)

    val temp: Int
        get() = current.getInt("temp")

    val windSpeed: Double
        get() = current.getDouble("wind_speed")

    val windDeg: Int
        get() = current.getInt("wind_deg")

    private val icon: String
        get() = weather0.getString("icon")

    private val iconUrlPrefix = "https://openweathermap.org/img/wn/"
    private val iconUrlSuffix = "@2x.png"

    val iconUrl: String
        get() = "$iconUrlPrefix$icon$iconUrlSuffix"
}