package org.igorkonyukhov.weather.data.restapi

object OpenWeatherIconUrlBuilder {
    private const val iconUrlPrefix = "https://openweathermap.org/img/wn/"
    private const val iconUrlSuffix = "@2x.png"

    fun buildUrlString(icon: String?) = if (icon != null) "$iconUrlPrefix$icon$iconUrlSuffix" else ""
}