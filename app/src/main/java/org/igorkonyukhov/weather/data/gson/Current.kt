package org.igorkonyukhov.weather.data.gson

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Current {
    @SerializedName("dt")
    @Expose
    var dt: Long? = null

    @SerializedName("sunrise")
    @Expose
    var sunrise: Long? = null

    @SerializedName("sunset")
    @Expose
    var sunset: Long? = null

    @SerializedName("temp")
    @Expose
    var temp: Int = 0

    @SerializedName("feels_like")
    @Expose
    var feelsLike: Double? = null

    @SerializedName("pressure")
    @Expose
    var pressure: Long? = null

    @SerializedName("humidity")
    @Expose
    var humidity: Long? = null

    @SerializedName("dew_point")
    @Expose
    var dewPoint: Double? = null

    @SerializedName("uvi")
    @Expose
    var uvi: Double? = null

    @SerializedName("clouds")
    @Expose
    var clouds: Long? = null

    @SerializedName("visibility")
    @Expose
    var visibility: Long? = null

    @SerializedName("wind_speed")
    @Expose
    var windSpeed: Double = 0.0

    @SerializedName("wind_deg")
    @Expose
    var windDeg: Int = 0

    @SerializedName("weather")
    @Expose
    var weather: List<Weather>? = null
}