package org.igorkonyukhov.weather.data.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import org.igorkonyukhov.weather.ui.viewmodel.Weather

@Entity(tableName = "weather")
data class WeatherEntity(
    @PrimaryKey @ColumnInfo(name = "id") override val id: Long = 0L,
    @ColumnInfo(name = "dt") override var dt: Long,
    @ColumnInfo(name = "temp") override var temp: Int,
    @ColumnInfo(name = "windSpeed") override var windSpeed: Double,
    @ColumnInfo(name = "windDeg") override var windDeg: Int,
    @ColumnInfo(name = "iconUrl") override var iconUrl: String
) : Weather