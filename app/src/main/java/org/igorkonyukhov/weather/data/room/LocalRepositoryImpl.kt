package org.igorkonyukhov.weather.data.room

import androidx.lifecycle.LiveData
import kotlinx.coroutines.*
import org.igorkonyukhov.weather.ui.viewmodel.LocalRepository
import org.igorkonyukhov.weather.ui.viewmodel.Weather
import javax.inject.Inject

class LocalRepositoryImpl @Inject constructor() :
    LocalRepository {
    @Inject
    lateinit var weatherDb: WeatherDatabase

    private val weatherDao by lazy {
        weatherDb.weatherDao()
    }

    @Suppress("UNCHECKED_CAST")
    override fun getAllData(): LiveData<List<Weather>> =
        weatherDao.getAll() as LiveData<List<Weather>>

    override fun insert(weather: Weather) {
        runBlocking {
            val job = launch(Dispatchers.IO) {
                weather as WeatherEntity
                weatherDao.insert(weather)
            }
            job.start()
            job.cancelAndJoin()
        }
    }

    override fun close() {
        weatherDb.close()
    }
}