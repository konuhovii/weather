package org.igorkonyukhov.weather.data.restapi

import org.igorkonyukhov.weather.data.gson.OpenWeatherJSON
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface OpenWeatherService {
    @GET("onecall")
    fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("exclude") exclude: String,
        @Query("units") units: String,
        @Query("apiKey") apiKey: String
    ): Call<OpenWeatherJSON>
}