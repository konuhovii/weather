package org.igorkonyukhov.weather

import android.app.Application
import org.igorkonyukhov.weather.di.AppComponent
import org.igorkonyukhov.weather.di.DaggerAppComponent

class WeatherApp : Application() {
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.factory().create(applicationContext, this)
    }
}