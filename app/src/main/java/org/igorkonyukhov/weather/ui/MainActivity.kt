package org.igorkonyukhov.weather.ui

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import org.igorkonyukhov.location.LocationHandler
import org.igorkonyukhov.weather.R
import org.igorkonyukhov.weather.WeatherApp
import org.igorkonyukhov.weather.databinding.ActivityMainBinding
import org.igorkonyukhov.weather.ui.viewmodel.MainActivityViewModel
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var mainActivityViewModel: MainActivityViewModel

    @Inject
    lateinit var imageLoader: ImageLoader

    private val locationHandler by lazy {
        LocationHandler(this)
    }

    private val sharedPreferences by lazy {
        getSharedPreferences(packageName, Context.MODE_PRIVATE)
    }

    private val firstTimeKey = "isFirstTime"

    private var isFirstTime: Boolean
        get() = sharedPreferences.getBoolean(firstTimeKey, true)
        set(value) = sharedPreferences.edit().putBoolean(firstTimeKey, value).apply()

    override fun onCreate(savedInstanceState: Bundle?) {
        (application as WeatherApp).appComponent.inject(this)

        super.onCreate(savedInstanceState)

        val dataBinding = DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        ).apply {
            viewModel = mainActivityViewModel
            lifecycleOwner = this@MainActivity
        }

        setSupportActionBar(dataBinding.toolbar)

        subscribeViewModel(dataBinding)
    }

    private fun subscribeViewModel(dataBinding: ActivityMainBinding) {
        mainActivityViewModel.iconUrl.observe(this, Observer { url ->
            imageLoader.loadUrlIntoImageView(url, dataBinding.weatherImage)
        })

        locationHandler.lastLocation.observe(this, Observer {
            mainActivityViewModel.lastLocation.value = it
        })

        mainActivityViewModel.lastLocation.observe(this, Observer { location ->
            location?.let {
                processLastLocation()
            }
        })
    }

    private fun processLastLocation() {
        if (isFirstTime) {
            mainActivityViewModel.currentWeather.value = null
            mainActivityViewModel.startPeriodicUpdates()
            isFirstTime = false
        } else {
            mainActivityViewModel.startPeriodicUpdates()
        }
    }

    override fun onStart() {
        super.onStart()
        try {
            locationHandler.checkLocationPermissionAndGoogleApiAvailability()
            showError("")
        } catch (e: Exception) {
            processLocationHandlerExceptions(e)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        try {
            locationHandler.processRequestPermissionsResult(requestCode, grantResults)
            showError("")
        } catch (e: Exception) {
            processLocationHandlerExceptions(e)
        }
    }

    private fun processLocationHandlerExceptions(e: Exception) {
        when (e) {
            is LocationHandler.GooglePlayServicesNotAvailableException -> {
                showError(
                    getString(R.string.google_play_services_not_available)
                )
            }
            is LocationHandler.LocationPermissionIsMissingException -> {
                showError(getString(R.string.location_permission_is_missing))
            }
        }
    }

    private fun showError(message: String) {
        mainActivityViewModel.errorText.postValue(message)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.update -> {
            mainActivityViewModel.updateWeatherData()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroy() {
        mainActivityViewModel.onDestroy()
        super.onDestroy()
    }
}