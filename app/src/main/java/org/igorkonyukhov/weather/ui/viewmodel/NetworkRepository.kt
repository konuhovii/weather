package org.igorkonyukhov.weather.ui.viewmodel

import org.igorkonyukhov.weather.data.gson.OpenWeatherJSON
import retrofit2.Response

interface NetworkRepository {
    suspend fun getCurrentWeather(
        lat: Double,
        lon: Double
    ): Response<OpenWeatherJSON>?

    fun cancelCurrentCall()
}