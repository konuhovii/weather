package org.igorkonyukhov.weather.ui.viewmodel

import androidx.lifecycle.LiveData

interface LocalRepository {
    fun getAllData(): LiveData<List<Weather>>

    fun insert(weather: Weather)

    fun close()
}