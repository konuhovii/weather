package org.igorkonyukhov.weather.ui.viewmodel

interface Weather {
    val id: Long
    var dt: Long
    var temp: Int
    var windSpeed: Double
    var windDeg: Int
    var iconUrl: String
}