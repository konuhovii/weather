package org.igorkonyukhov.weather.ui.viewmodel

import android.app.Application
import androidx.lifecycle.Observer
import org.igorkonyukhov.weather.R
import org.igorkonyukhov.weather.util.WindDirectionConverter
import java.text.SimpleDateFormat
import java.util.*

class CurrentWeatherObserver(private val viewModel: MainActivityViewModel) : Observer<Weather?> {
    private val app: Application
        get() = viewModel.getApplication()

    private val dateTimeFormatter = SimpleDateFormat.getDateTimeInstance(
        SimpleDateFormat.DEFAULT,
        SimpleDateFormat.DEFAULT,
        Locale.getDefault()
    )

    override fun onChanged(weather: Weather?) {
        when {
            weather != null -> {
                viewModel.updateStatus.value =
                    app.getString(R.string.last_updated, dateTimeFormatter.format(weather.dt))

                viewModel.temperature.value = app.getString(R.string.temperature, weather.temp)

                viewModel.wind.value = app.getString(
                    R.string.wind, weather.windSpeed,
                    WindDirectionConverter.convert(weather.windDeg)
                )

                viewModel.iconUrl.value = weather.iconUrl
            }
            else ->
                viewModel.updateStatus.value = app.getString(R.string.no_data)
        }
    }
}