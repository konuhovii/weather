package org.igorkonyukhov.weather.ui.viewmodel

interface TaskRepeater {
    fun repeatTask(delayAfter: Long, task: suspend () -> Unit)
    fun cancelTask()
}