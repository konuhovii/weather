package org.igorkonyukhov.weather.ui.viewmodel

import kotlinx.coroutines.CoroutineScope
import org.igorkonyukhov.weather.util.TaskRepeaterImpl

fun getTaskRepeater(scope: CoroutineScope): TaskRepeater {
    return TaskRepeaterImpl.getInstance(scope)
}