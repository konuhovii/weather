package org.igorkonyukhov.weather.ui

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import javax.inject.Inject

class ImageLoaderImpl @Inject constructor(context: Context) : ImageLoader {
    private val requestManager = Glide.with(context)

    override fun loadUrlIntoImageView(url: String, imageView: ImageView) {
        requestManager.load(url).into(imageView)
    }
}