package org.igorkonyukhov.weather.ui

import android.widget.ImageView

interface ImageLoader {
    fun loadUrlIntoImageView(url: String, imageView: ImageView)
}