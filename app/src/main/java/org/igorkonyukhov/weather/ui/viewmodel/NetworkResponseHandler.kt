package org.igorkonyukhov.weather.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.igorkonyukhov.weather.data.gson.OpenWeatherJSON
import org.igorkonyukhov.weather.data.restapi.OpenWeatherIconUrlBuilder
import org.igorkonyukhov.weather.data.room.WeatherEntity
import org.json.JSONException
import retrofit2.Response

object NetworkResponseHandler {
    private val _errorText = MutableLiveData<String>()

    val errorText: LiveData<String> = _errorText

    fun handleResponse(
        response: Response<OpenWeatherJSON>,
        localRepository: LocalRepository
    ) = synchronized(this) {
        if (isResponseFailed(response))
            return@synchronized
        response.body()?.let { responseBody ->
            try {
                processResponse(responseBody, localRepository)
                _errorText.postValue("")
            } catch (e: JSONException) {
                _errorText.postValue(e.message)
            }
        }
    }

    private fun isResponseFailed(response: Response<OpenWeatherJSON>): Boolean {
        return if (!response.isSuccessful) {
            _errorText.postValue(response.message())
            true
        } else false
    }

    @Throws(JSONException::class)
    private fun processResponse(
        openWeatherJSON: OpenWeatherJSON,
        localRepository: LocalRepository
    ) {
        openWeatherJSON.current?.let { current ->
            val weather0 = current.weather!![0]
            val weather = WeatherEntity(
                dt = System.currentTimeMillis(),
                temp = current.temp,
                windSpeed = current.windSpeed,
                windDeg = current.windDeg,
                iconUrl = OpenWeatherIconUrlBuilder.buildUrlString(weather0.icon)
            )
            localRepository.insert(weather)
        }
    }
}