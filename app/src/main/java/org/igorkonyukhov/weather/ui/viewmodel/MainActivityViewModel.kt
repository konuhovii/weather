package org.igorkonyukhov.weather.ui.viewmodel

import android.app.Application
import android.location.Location
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.igorkonyukhov.weather.R
import javax.inject.Inject

class MainActivityViewModel @Inject constructor(
    app: Application,
    val localRepository: LocalRepository,
    val networkRepository: NetworkRepository
) : AndroidViewModel(app) {

    val lastLocation = MutableLiveData<Location?>()
    val isUpdateInProgress = MutableLiveData<Boolean>()

    val temperature = MutableLiveData<String>()

    val wind = MutableLiveData<String>()

    val iconUrl = MutableLiveData<String>()

    val errorText = MutableLiveData<String>()

    val updateStatus = MutableLiveData<String>()

    val currentWeather = MutableLiveData<Weather?>()

    private val app = getApplication<Application>()

    private val currentWeatherObserver: Observer<Weather?> =
        CurrentWeatherObserver(this)

    private val repositoryDataObserver = Observer<List<Weather>> { list ->
        if (list.isNotEmpty()) {
            currentWeather.value = list.last()
        }
    }

    private val networkResponseErrorTextObserver = Observer<String> {
        errorText.value = it
    }

    init {
        currentWeather.observeForever(currentWeatherObserver)
        localRepository.getAllData().observeForever(repositoryDataObserver)
        NetworkResponseHandler.errorText.observeForever(networkResponseErrorTextObserver)
    }

    fun updateWeatherData() = synchronized(this) {
        if (lastLocation.value != null)
            handleLocationNotNull()
        else
            handleLocationIsNull()
    }

    private fun handleLocationIsNull() {
        errorText.postValue(
            app.getString(R.string.location_permission_is_missing)
        )
    }

    private fun handleLocationNotNull() {
        errorText.postValue("")
        val location = lastLocation.value!!
        viewModelScope.launch(Dispatchers.IO) {
            isUpdateInProgress.postValue(true)
            networkRepository.getCurrentWeather(location.latitude, location.longitude)
                ?.let { response ->
                    NetworkResponseHandler.handleResponse(response, localRepository)
                }
            isUpdateInProgress.postValue(false)
        }
    }

    private val updateWeatherDelay = 600000L // 10 minutes
    private val taskRepeater = getTaskRepeater(viewModelScope)

    fun startPeriodicUpdates() {
        taskRepeater.repeatTask(updateWeatherDelay) {
            updateWeatherData()
        }
    }

    fun onDestroy() {
        taskRepeater.cancelTask()
        networkRepository.cancelCurrentCall()
        currentWeather.removeObserver(currentWeatherObserver)
        localRepository.getAllData().removeObserver(repositoryDataObserver)
        localRepository.close()
    }
}
