package org.igorkonyukhov.weather.util

import kotlin.math.round

object WindDirectionConverter {
    private val compassDirections = arrayOf(
        "North",
        "North-East",
        "East",
        "South-East",
        "South",
        "South-West",
        "West",
        "North-West"
    )

    fun convert(degrees: Int): String {
        val count = compassDirections.size
        val split = degrees.toDouble() * count / 360
        val round = round(split).toInt()
        val i = (round + count) % count
        return compassDirections[i]
    }
}