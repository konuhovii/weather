package org.igorkonyukhov.weather.util

import kotlinx.coroutines.*
import org.igorkonyukhov.weather.ui.viewmodel.TaskRepeater

class TaskRepeaterImpl private constructor(private val scope: CoroutineScope) : TaskRepeater {
    var currentJob: Job? = null

    override fun repeatTask(delayAfter: Long, task: suspend () -> Unit) {
        currentJob = scope.launch(Dispatchers.IO) {
            while (isActive) {
                task()
                delay(delayAfter)
            }
        }
    }

    override fun cancelTask() {
        runBlocking {
            currentJob?.cancelAndJoin()
        }
    }

    companion object {
        private val instance: TaskRepeater? = null

        fun getInstance(scope: CoroutineScope): TaskRepeater = synchronized(this) {
            instance ?: TaskRepeaterImpl(scope)
        }
    }
}