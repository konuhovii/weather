package org.igorkonyukhov.location

import android.Manifest
import android.content.Context

internal class CheckLocationPermission(context: Context) : CheckPermission(context) {
    override val permission = Manifest.permission.ACCESS_COARSE_LOCATION
}