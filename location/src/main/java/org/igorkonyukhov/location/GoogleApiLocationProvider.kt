package org.igorkonyukhov.location

import android.annotation.SuppressLint
import android.location.Location
import com.google.android.gms.location.FusedLocationProviderClient

internal class GoogleApiLocationProvider(private val fusedLocationClient: FusedLocationProviderClient) :
    LocationProvider {
    @SuppressLint("MissingPermission")
    override fun getLastLocation(callback: (Location?) -> Unit) {
        fusedLocationClient.lastLocation.addOnSuccessListener {
            callback(it)
        }
    }
}