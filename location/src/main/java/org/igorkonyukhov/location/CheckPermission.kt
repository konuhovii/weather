package org.igorkonyukhov.location

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

abstract class CheckPermission(private val context: Context) {
    abstract val permission: String

    val permissionGranted: Boolean
        get() = ContextCompat.checkSelfPermission(
            context,
            permission
        ) == PackageManager.PERMISSION_GRANTED
}