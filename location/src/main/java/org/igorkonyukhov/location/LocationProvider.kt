package org.igorkonyukhov.location

import android.location.Location

interface LocationProvider {
    fun getLastLocation(callback: (Location?) -> Unit)
}