package org.igorkonyukhov.location

import android.content.Context
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability

internal class CheckGooglePlayServices(private val context: Context) {
    private val googleApiAvailability: GoogleApiAvailability = GoogleApiAvailability.getInstance()

    private val googlePlayServicesNotAvailable = setOf(
        ConnectionResult.SERVICE_MISSING,
        ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,
        ConnectionResult.SERVICE_DISABLED,
        ConnectionResult.SERVICE_INVALID
    )

    val notAvailable: Boolean
        get() = googleApiAvailability.isGooglePlayServicesAvailable(context) in googlePlayServicesNotAvailable
}