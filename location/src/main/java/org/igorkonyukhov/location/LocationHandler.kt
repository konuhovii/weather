package org.igorkonyukhov.location

import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import androidx.core.app.ActivityCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.LocationServices
import kotlin.jvm.Throws

class LocationHandler(private val activity: Activity) {
    private val locationPermissionRequestCode = 10001

    private val checkLocationPermission: CheckPermission =
        CheckLocationPermission(activity.applicationContext)

    private val checkGooglePlayServices = CheckGooglePlayServices(activity.applicationContext)

    private val googleApiLocationProvider: LocationProvider

    init {
        val fusedLocationClient =
            LocationServices.getFusedLocationProviderClient(activity.applicationContext)
        googleApiLocationProvider = GoogleApiLocationProvider(fusedLocationClient)
    }

    @Throws(GooglePlayServicesNotAvailableException::class)
    fun checkLocationPermissionAndGoogleApiAvailability() {
        val requiredPermissions = mutableListOf<String>()
        if (!checkLocationPermission.permissionGranted) {
            requiredPermissions.add(checkLocationPermission.permission)
            requestPermissions(requiredPermissions, locationPermissionRequestCode)
        } else {
            checkGoogleApiAvailability()
        }
    }

    private fun requestPermissions(
        requiredPermissions: List<String>,
        requestCode: Int
    ) {
        ActivityCompat.requestPermissions(
            activity,
            requiredPermissions.toTypedArray(),
            requestCode
        )
    }

    @Throws(
        LocationPermissionIsMissingException::class,
        GooglePlayServicesNotAvailableException::class
    )
    fun processRequestPermissionsResult(requestCode: Int, grantResults: IntArray) {
        if (requestCode == locationPermissionRequestCode)
            processLocationPermission(grantResults)
    }

    @Throws(
        LocationPermissionIsMissingException::class,
        GooglePlayServicesNotAvailableException::class
    )
    private fun processLocationPermission(grantResults: IntArray) {
        if (grantResults.isNotEmpty()
            && grantResults[0] == PackageManager.PERMISSION_GRANTED
        ) {
            checkGoogleApiAvailability()
        } else {
            throw LocationPermissionIsMissingException()
        }
    }

    @Throws(GooglePlayServicesNotAvailableException::class)
    private fun checkGoogleApiAvailability() {
        if (checkGooglePlayServices.notAvailable) {
            throw GooglePlayServicesNotAvailableException()
        } else {
            getLastLocation()
        }
    }

    private val _lastLocation = MutableLiveData<Location>()

    val lastLocation: LiveData<Location> = _lastLocation

    private fun getLastLocation() {
        googleApiLocationProvider.getLastLocation { location ->
            location?.let {
                _lastLocation.postValue(location)
            }
        }
    }

    class GooglePlayServicesNotAvailableException : Exception() {
        override val message: String?
            get() = "Google Play Services is not available on your device or not updated."
    }

    class LocationPermissionIsMissingException : Exception() {
        override val message: String?
            get() = "Location permission is missing."
    }
}